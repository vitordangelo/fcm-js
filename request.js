const http = require('https')

const options = {
  hostname: 'fcm.googleapis.com',
  port: null,
  path: '/fcm/send',
  method: 'POST',
  headers: {
    'content-type': 'application/json',
    'authorization': 'key=AIzaSyBIFit_FsfBlAQA1DFRQxsH4lj9rkMccOc'
  }
}

const req = http.request(options, (res) => {
  var chunks = []
  res.on('data', function (chunk) {
    chunks.push(chunk)
  })

  res.on('end', function () {
    var body = Buffer.concat(chunks)
    console.log(body.toString())
  })
})

req.on('error', (e) => {
  console.error(`problem with request: ${e.message}`)
})

req.write(JSON.stringify({ notification:
  { title: 'Alarme',
    body: 'Alarme disparado. Pelo amor de Deus chama a puliça',
    icon: 'https://i.ytimg.com/vi/s0m7lbHTGfY/hqdefault.jpg' },
registration_ids:
  [ 'eMHH0LmR_w4:APA91bFeHlKO80Eyfp53wHtvt0sMxGLfVxWlxMakO3JW9HvE4uHfmLTIanCkdIk8FYZ8VPq1v2ozAjkpbmtUO8Tij0CD4T1i8j8X-UmMmm_i3uDRAy8KI1w9L23wflL-ek1OrPKZFfSY',
    'cNZLJFsxCp8:APA91bEwIKMsK696pGDSX9wVYuoFrnKfrO3LDwhZI8Zvf-zWzBZWkSNidrxbkfo2aYBEyUudrQTWiT0qD-U2FdajVxRd3YyDKxIJQsUT1xlaVMr9XvWwy6xJU3z5OB7PC4rDEltub5Ny',
    'dE3HKImWRGI:APA91bHTDNF38OaoiGiAaWiuHQad90PhPkGe40kddbaqKhd2ZStT8pKOZpuom25C-2DYjTpSY2DpQdOTU1XPIlII0y-CtyBm9g58psdabv00v61SvWnlhSwxaxb-HrKkdIUkNys9hkAh',
    'cqMEkfLCmC8:APA91bF9L9042OESkMSEdbRBiF4kd7R5XwSw3ViieVApfqVRs_Bpazh9WWLl23m9tLqRosrpIEXxrnD_AxGkyl-pKwHb7dQFEDEQX0Io2m-zrVLM91P5uao_T_W5JbMBF5kkE3JgLVKo',
    'cnPMfkxhPJ4:APA91bFVzQPMe1EP2mBJmFVWJrifwiWACMWZ9X_X5Hw5MHCVS3PjGa3xO3e3x1NLRzFmNC0wkpxeXGYGTipiLkXTnDaCnkGP9vHm-YGQIqkti2UilHxPJGtWE4g8MxjXBujcnZoHe7WC' ] }))
req.end()
